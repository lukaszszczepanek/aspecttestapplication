package testapplication.Model;

import org.springframework.stereotype.Component;

@Component
public class Stub {

	public  Wallet depositMoney(int money, Wallet wallet) throws Exception {

		if (wallet.getAccountBallance() < money) {
			throw new Exception("No enougth money in account.");
		} else {
			wallet.setWalletBallance(wallet.getWalletBallance() + money);
			wallet.setAccountBallance(wallet.getAccountBallance() - money);
		}
		return wallet;
	}

	public  Wallet payMoney(int money, Wallet wallet) throws Exception {

		if (money > wallet.getWalletBallance()) {
			throw new Exception("No enougth money");
		} else {
			wallet.setWalletBallance(wallet.getWalletBallance() - money);
		}
		return wallet;
	}

	public  Wallet accountBallance(Wallet wallet) {
		return wallet;
	}
}
