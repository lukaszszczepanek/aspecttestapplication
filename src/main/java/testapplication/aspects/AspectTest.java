package testapplication.aspects;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;

import org.springframework.stereotype.Component;

@Aspect
@Component
public class AspectTest {

//	Logger logger = 

	@Pointcut("execution(* testapplication..*(..))")
	public void pointcut() {};
	
	@Pointcut("execution(* testapplication.App.*(..))")
	public void pointcut2() {};
	
	@Before("pointcut() and !pointcut2()")
	public void entering(JoinPoint joinPoint) {

//		logger.trace("Entering method:");
//		logger.trace(joinPoint.getSignature().toLongString());
//		logger.trace(joinPoint.getSignature().toShortString());
//		logger.trace(joinPoint.getSignature().toString());

		for (Object arg : joinPoint.getArgs()) {
			System.out.println("Method: " + joinPoint.getSignature() + "\nArgs: " + arg);
		}

		// System.err.println("Method: "+ joinPoint.getSignature()
		// +"\nParameters: "+ wallet);

	}

	@AfterReturning(pointcut = "pointcut() && !pointcut2()", returning = "object")
	public void after(JoinPoint joinpoint, Object object) {
		System.out.println("Retunred object by method " + joinpoint.getSignature() + " \n" + object);
	}

}
