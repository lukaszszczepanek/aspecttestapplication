package testapplication.Model;


public class Wallet {

	private int accountBallance;
	private int walletBallance;

	public int getWalletBallance() {
		return walletBallance;
	}

	public void setWalletBallance(int walletBallance) {
		this.walletBallance = walletBallance;
	}

	public int getAccountBallance() {
		return accountBallance;
	}

	public void setAccountBallance(int accountBallance) {
		this.accountBallance = accountBallance;
	}

	public Wallet(int accountBallance, int walletBallance) {
		super();
		this.accountBallance = accountBallance;
		this.walletBallance = walletBallance;
	}

	@Override
	public String toString() {
		return "Wallet [accountBallance=" + accountBallance + ", walletBallance=" + walletBallance + "]";
	}

	

}
