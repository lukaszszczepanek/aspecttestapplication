package testapplication.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import testapplication.Model.Stub;
import testapplication.Model.Wallet;



@RestController
public class MainController {

	Wallet wallet = new Wallet(100, 0);
	Stub stub = new Stub();
	
	@RequestMapping(value = "deposit/{money}", method = RequestMethod.POST)
	public Wallet depositMoney(@PathVariable int money) throws Exception{
		
		return stub.depositMoney(money, wallet);
	}
	
	@RequestMapping(value = "pay/{money}", method = RequestMethod.POST)
	public Wallet payMoney(@PathVariable int money) throws Exception{
		return stub.payMoney(money, wallet);
	}

	@RequestMapping(value = "wallet", method = RequestMethod.GET)
	public Wallet accountBallance(){
		return stub.accountBallance(wallet);

	}
	
}
